const http = require("http");

const port = 3001;

const server = http.createServer(function (req, res) {

	if(req.url == "/greeting"){
		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end("Hello Again")

	} else if(req.url == "/Homepage") {
		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end("Welcome Home")
	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'});

		res.end("Page Not Found!")
	}
})

server.listen(port);

console.log(`Server is now accessible at localhost:${port}`);